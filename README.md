Prototyping a dumb server for location sharing apps
===================================================

This is a code repository that goes along the article
[http://gradha.github.io/articles/2018/04/prototyping-a-dumb-server-for-location-sharing-apps.html](http://gradha.github.io/articles/2018/04/prototyping-a-dumb-server-for-location-sharing-apps.html).
It describes a hypothetical protocol for mobile clients willing to share their
geographic position in an encrypted fashion such that the server coordinating
their communication can't easily see their communications.

The source code is implemented in ANSI C, so should compile in most places. The
only external dependency is the [Sodium crypto
library](https://download.libsodium.org/doc/) which you can install locally or
globally. The provided ``Makefile`` uses local hard-coded paths so it likely
won't be of any use to use, but compilation should be a matter of typing ``gcc
-o binary -lsodium *.c`` with possibly a few other additional switches to
locate the path to the libsodium binary.

Nothing in this repository should be trusted. I have no idea of any crypto.
