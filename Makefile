CC=gcc
CFLAGS=-I$(HOME)/instalacion_manual/local/include \
	-L$(HOME)/instalacion_manual/local/lib \
	-Iexternal/NibbleAndAHalf/NibbleAndAHalf
EXE=simulate.exe

all:
	gcc -ansi -Wall $(CFLAGS) -lsodium -o $(EXE) *.c

.PHONY: clean run

clean:
	rm -f *.exe *.o

run: all
	./$(EXE)

#  vim: set ts=4 sw=4 tw=0 noet :
