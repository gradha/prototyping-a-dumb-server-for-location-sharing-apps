#include "server.h"

#include "misc.h"

#include <assert.h>
#include <sodium.h>
#include <time.h>

static void increase_session_time(Session* session)
{
	session->current_time += (long)randombytes_random;
}

/**
 * Initializes the specified session with the id and marks it as used.
 *
 * Returns the input session pointer.
 */
static Session* init_session(Session* session, unsigned char* session_id)
{
	assert(!session->used);
	session->used = 1;
	session->current_time = time(0) * 1000;
	copy_session_id(session_id, session->id);
	return session;
}

/**
 * Returns the session or NULL if none could be created.
 */
Session* open_session(Server* server, unsigned char* session_id)
{
	int f;
	/* First look for an existing session. */
	for (f = 0; f < MAX_SESSIONS; f++) {
		if (server->session[f].used) {
			if (0 == compare_session_id(server->session[f].id, session_id)) {
				increase_session_time(&server->session[f]);
				return &server->session[f];
			}
		}
	}

	/* Assign the first open entry you can find.*/
	for (f = 0; f < MAX_SESSIONS; f++) {
		if (!server->session[f].used) {
			return init_session(&server->session[f], session_id);
		}
	}

	return NULL;
}

/**
 * Registers a slot in the session, giving it a random 32bit value, which is
 * returned.
 *
 * Returns zero on failure (this is not specified in the original protocol, but
 * avoids us fancier error handling/communication now).
 */
unsigned int register_random_chat_id(Session* session)
{
	int f;

	/* Assign the first open entry you can find.*/
	for (f = 0; f < MAX_CLIENTS; f++) {
		if (!session->clients[f].used) {
			Client_info* c = &session->clients[f];
			const unsigned int id = 1 + randombytes_uniform(0xffffff);
			c->used = 1;
			c->id = id;
			increase_session_time(session);

			printf("{'a': 'logged_in', 'id': %u, 't': %lu}\n",
				id, session->current_time * 1000L);

			/* Also notify other clients of the new user. */
			for (int g = 0; g < MAX_CLIENTS; g++) {
				if (g != f && session->clients[g].used) {
					printf("to client %u: {'a': 'new_user', 'id': %u}\n",
						session->clients[g].id, id);
				}
			}

			return id;
		}
	}

	return 0;
}

void publish_encrypted(Session* session,
	const char* payload, long timestamp, int from)
{
	printf("Server received encrypted JSON: {'p': '%s', 't': %lu, 'from': %u}\n",
		payload, timestamp, from);

	int f;
	for (f = 0; f < MAX_CLIENTS; f++) {
		if (session->clients[f].used && session->clients[f].id != from) {
			printf("\tJSON sent to client %u\n", session->clients[f].id);
		}
	}

	increase_session_time(session);
}

/* vim: set ts=4 sw=4 tw=0 noet :*/
