#include "client.h"
#include "misc.h"
#include "server.h"

#include <sodium.h>
#include <stdio.h>
#include <assert.h>

Server server;
Client_state c1, c2, c3;

/**
 * Emulates creation of a session and several clients talking to each other
 * over a ficticious server.
 */
int main(void)
{
	if (sodium_init() < 0) {
		printf("panic! the library couldn't be initialized, not safe to use\n");
		return -1;
	}

	gen_random_session_id(c1.session_id);
	gen_random_encryption_key(c1.encryption_key);
	printf("Got session id %s (base64: %s)\n",
		session_id_to_hex(c1.session_id), session_id_to_base64(c1.session_id));

	printf("The encryption key is %s (base64: %s)\n",
		encryption_key_to_hex(c1.encryption_key),
		encryption_key_to_base64(c1.encryption_key)
		);

	printf("A hypothetical URL for web clients could be:\n\t"
		"https://server.com:1234/path?s=%s#%s\n\n",
		session_id_to_hex(c1.session_id), encryption_key_to_hex(c1.session_id));

	/* Copy the values to the second client, er… through whatsapp. */
	copy_session_id(c1.session_id, c2.session_id);
	copy_encryption_key(c1.encryption_key, c2.encryption_key);
	assert(0 == compare_session_id(c1.session_id, c2.session_id));
	assert(0 == compare_encryption_key(c1.encryption_key, c2.encryption_key));
	copy_session_id(c1.session_id, c3.session_id);
	copy_encryption_key(c1.encryption_key, c3.encryption_key);

	Session* s1 = open_session(&server, c1.session_id);
	Session* s2 = open_session(&server, c2.session_id);
	Session* s3 = open_session(&server, c3.session_id);
	assert(s1 == s2); /* Same session should be returned for same id. */
	assert(s2 == s3); /* Same session should be returned for same id. */

	/* Obtain clients identifier for the chat. */
	c1.chat_id = register_random_chat_id(s1);
	c1.current_time = s1->current_time;
	c2.chat_id = register_random_chat_id(s2);
	c2.current_time = s2->current_time;
	c3.chat_id = register_random_chat_id(s3);
	c3.current_time = s3->current_time;

	printf("Client ids: 1:%d, 2:%d: 3:%d\n\n",
		c1.chat_id, c2.chat_id, c3.chat_id);

	c1.current_time = s1->current_time;
	Client_message* m = gen_broadcast_pos(&c1, 43.2f, 15.935f);
	publish_encrypted(s1,
		ciphertext_to_base64(m->ciphertext_json, m->ciphertext_length),
		m->timestamp, c1.chat_id);

	decrypt_message(m, c2.encryption_key);

	printf("\nsecretbox bytes %d\n", crypto_secretbox_KEYBYTES);
	printf("secretbox nonce bytes %d\n", crypto_secretbox_NONCEBYTES);
	printf("secretbox mac bytes %d\n", crypto_secretbox_MACBYTES);
	printf("Simulate EOF\n");
	return 0;
}

/* vim: set ts=4 sw=4 tw=0 noet :*/
