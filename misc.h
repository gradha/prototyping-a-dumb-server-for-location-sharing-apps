#ifndef _MISC_H_
#define _MISC_H_

#define SESSION_ID_LEN (128/8) /* Number of bytes used for the session id. */

/* Length of the encryption key generated and shared with other users. */
#define ENCRYPTION_KEY_LEN 32

/* Random big value for strings, usually for console output. */
#define MAX_BUF 512

const char* session_id_to_hex(unsigned char* session_id);
const char* encryption_key_to_hex(unsigned char* session_id);
const char* session_id_to_base64(unsigned char* session_id);
const char* encryption_key_to_base64(unsigned char* session_id);
const char* ciphertext_to_hex(unsigned char* text, unsigned long len);
const char* ciphertext_to_base64(unsigned char* text, unsigned long len);
int compare_session_id(unsigned char* id1, unsigned char* id2);
int compare_encryption_key(unsigned char* key1, unsigned char* key2);
void copy_session_id(unsigned char* src, unsigned char* dest);
void copy_encryption_key(unsigned char* src, unsigned char* dest);

#endif /* _MISC_H_ */

/* vim: set ts=4 sw=4 tw=0 noet :*/
