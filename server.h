#ifndef _SERVER_H_
#define _SERVER_H_

#include "misc.h"

#define MAX_CLIENTS 10
#define MAX_SESSIONS 5

typedef struct Client_info {
	char used; /* True if this entry in the array is used. */
	unsigned int id; /* The random session identifier given to the client. */
} Client_info;

typedef struct Session {
	char used; /* True if this entry in the array is used. */
	unsigned char id[SESSION_ID_LEN]; /* The session id. */
	Client_info clients[MAX_CLIENTS]; /* All the possible clients listening. */
	long current_time; /* The presumed current time of the server, as ms. */
} Session;

/**
 * Just a basic array holding everything statically.
 *
 * Note the `bool used` entries everywhere, they allow us to preallocate all
 * the structures and only mark entries used/unused to avoid having to deal
 * with pointers, memory allocation and other hard things in life.
 */
typedef struct Server {
	Session session[MAX_SESSIONS];
} Server;

Session* open_session(Server* server, unsigned char* session_id);
unsigned int register_random_chat_id(Session* session);

void publish_encrypted(Session* session,
	const char* payload, long timestamp, int from);

#endif /* _SERVER_H_ */

/* vim: set ts=4 sw=4 tw=0 noet :*/
