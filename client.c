#include "client.h"

#include "misc.h"

#include <assert.h>
#include <sodium.h>
#include <string.h>

void gen_random_session_id(unsigned char* buf)
{
	randombytes_buf(buf, SESSION_ID_LEN);
}

void gen_random_encryption_key(unsigned char* buf)
{
	randombytes_buf(buf, ENCRYPTION_KEY_LEN);
}

static Client_message g_message;

/**
 * Generates a nonce from the current time and chat_id. This essentially resets
 * the nonce to zero and writes the current time and chat id as bytes directly.
 */
void gen_nonce(unsigned char* nonce, long current_time, int chat_id)
{
	assert(crypto_secretbox_NONCEBYTES > 12);
	memset(nonce, 0, crypto_secretbox_NONCEBYTES);
	nonce[0] = (current_time & 0x00000000000000FF) >> 0;
	nonce[1] = (current_time & 0x000000000000FF00) >> 8;
	nonce[2] = (current_time & 0x0000000000FF0000) >> 16;
	nonce[3] = (current_time & 0x00000000FF000000) >> 24;
	nonce[4] = (current_time & 0x000000FF00000000) >> 32;
	nonce[5] = (current_time & 0x0000FF0000000000) >> 40;
	nonce[6] = (current_time & 0x00FF000000000000) >> 48;
	nonce[7] = (current_time & 0xFF00000000000000) >> 56;
	nonce[8] =  (chat_id & 0x000000FF) >> 0;
	nonce[9] =  (chat_id & 0x0000FF00) >> 8;
	nonce[10] = (chat_id & 0x00FF0000) >> 16;
	nonce[11] = (chat_id & 0xFF000000) >> 24;
}

/**
 * Returns a pointer to a non reusable buffer containing a client message.
 */
Client_message* gen_broadcast_pos(Client_state* state, float lat, float lon)
{
	sprintf(g_message.plaintext_json,
		"{'a': 'pos', 'lat': %0.6f, 'lon': %0.6f}", lat, lon);
	/* The +1 is to include the string terminating null byte. */
	unsigned long long len = strlen(g_message.plaintext_json) + 1;
	g_message.ciphertext_length = len + crypto_secretbox_MACBYTES;
	g_message.timestamp = state->current_time;
	g_message.from_chat_id = state->chat_id;

	/* To encrypt the payload we need a nonce from the client's state. */
	unsigned char nonce[crypto_secretbox_NONCEBYTES];
	gen_nonce(nonce, state->current_time, state->chat_id);

	crypto_secretbox_easy(g_message.ciphertext_json,
		(void*)g_message.plaintext_json, len,
		nonce, state->encryption_key);

	printf("client %u wants to send: %s\n",
		state->chat_id, g_message.plaintext_json);

	printf("The encrypted payload is %lu bytes:\n\thex: %s\n\tbase64: %s\n",
		g_message.ciphertext_length,
		ciphertext_to_hex(g_message.ciphertext_json,
			g_message.ciphertext_length),
		ciphertext_to_base64(g_message.ciphertext_json,
			g_message.ciphertext_length)
		);

	return &g_message;
}

/**
 * Using only the ciphertext, length, timestamp, and chat id, tries to decript
 * the contents and print them to stdout.
 *
 * Of course the plaintext is there, but we are trusting ourselves, aren't we?
 */
void decrypt_message(Client_message* message, unsigned char* encryption_key)
{
	/* Rebuild the nonce from the sender identity and message time. */
	unsigned char nonce[crypto_secretbox_NONCEBYTES];
	gen_nonce(nonce, message->timestamp, message->from_chat_id);

	unsigned char decrypted[MAX_BUF];

	if (0 != crypto_secretbox_open_easy(decrypted, message->ciphertext_json,
		message->ciphertext_length, nonce, encryption_key)) {

		printf("\n\n\tOoops, couldn't decrypt the message!\n");
		return;
	}

	/* We encrypt including the string null terminator, so just printf it. */
	printf("Client decrypted '%s'\n", decrypted);
}

/* vim: set ts=4 sw=4 tw=0 noet :*/
