#ifndef _CLIENT_H_
#define _CLIENT_H_

#include "misc.h"

/** Holds the current client state, like session, encryption key and id */
typedef struct Client_state {
	unsigned char session_id[SESSION_ID_LEN];
	unsigned char encryption_key[ENCRYPTION_KEY_LEN];
	int chat_id; /* The value assigned by the server */
	long current_time; /* The current time, used for each message, as ms. */
} Client_state;

typedef struct Client_message {
	char plaintext_json[MAX_BUF]; /* The stored plaintext version. */
	unsigned char ciphertext_json[MAX_BUF]; /* The encrypted version. */
	unsigned long ciphertext_length; /* Length of the binary cipher bytes. */
	int from_chat_id; /* The chat id of the message originator. */
	long timestamp; /* The time at which the message was constructed. */
} Client_message;

void gen_random_session_id(unsigned char* buf);
void gen_random_encryption_key(unsigned char* buf);
Client_message* gen_broadcast_pos(Client_state* state, float lat, float lon);
void decrypt_message(Client_message* message, unsigned char* encryption_key);

#endif /* _CLIENT_H_ */

/* vim: set ts=4 sw=4 tw=0 noet :*/
