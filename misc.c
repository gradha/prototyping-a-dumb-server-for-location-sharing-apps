#include "misc.h"

#include "base64.h"

#include <sodium.h>
#include <assert.h>
#include <string.h>

static char g_session_id[SESSION_ID_LEN * 2 + 1];
static char g_encryption_key[ENCRYPTION_KEY_LEN * 2 + 1];
static char g_ciphertext[MAX_BUF];
static char g_ciphertext_b64[MAX_BUF];
static char g_session_b64[MAX_BUF];
static char g_encryption_b64[MAX_BUF];

static char value_to_char[16] = {
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
};

/** Generic buffer to hex converter */
void buf_to_hex(unsigned char* src, char* dest, int len)
{
	while (len-- > 0) {
		const unsigned char value = *src++;
		*dest++ = value_to_char[(value & 0xF0) >> 4];
		*dest++ = value_to_char[(value & 0x0F)];
	}
	*dest = 0;
}

/** Uses a global (non reentrant!) buffer to transform the ID into a hex string.
 *
 * The returned string is null terminated.
 */
const char* session_id_to_hex(unsigned char* session_id)
{
	buf_to_hex(session_id, g_session_id, SESSION_ID_LEN);
	return g_session_id;
}

/** Uses a global (non reentrant!) buffer to transform the ID into a hex string.
 *
 * The returned string is null terminated.
 */
const char* encryption_key_to_hex(unsigned char* session_id)
{
	assert(ENCRYPTION_KEY_LEN == crypto_secretbox_KEYBYTES);
	buf_to_hex(session_id, g_encryption_key, ENCRYPTION_KEY_LEN);
	return g_encryption_key;
}

const char* ciphertext_to_hex(unsigned char* text, unsigned long len)
{
	assert(len * 2 + 1 < MAX_BUF);
	buf_to_hex(text, g_ciphertext, len);
	return g_ciphertext;
}

/** Uses a global (non reentrant!) buffer to transform the ID into base64.
 *
 * The returned string is null terminated.
 */
const char* session_id_to_base64(unsigned char* session_id)
{
	int out_len;
	char* b = base64(session_id, SESSION_ID_LEN, &out_len);
	strcpy(g_session_b64, b);
	free(b);
	g_session_b64[out_len] = 0;
	assert(out_len < MAX_BUF);

	return g_session_b64;
}

/** Uses a global (non reentrant!) buffer to transform the ID into base64.
 *
 * The returned string is null terminated.
 */
const char* encryption_key_to_base64(unsigned char* session_id)
{
	assert(ENCRYPTION_KEY_LEN == crypto_secretbox_KEYBYTES);
	int out_len;
	char* b = base64(session_id, ENCRYPTION_KEY_LEN, &out_len);
	strcpy(g_encryption_b64, b);
	free(b);
	g_encryption_b64[out_len] = 0;
	assert(out_len < MAX_BUF);

	return g_encryption_b64;
}

/** Uses a global (non reentrant!) buffer to transform the cipher into base64.
 *
 * The returned string is null terminated.
 */
const char* ciphertext_to_base64(unsigned char* text, unsigned long len)
{
	int out_len;
	char* b = base64(text, len, &out_len);
	assert(out_len < MAX_BUF);
	strcpy(g_ciphertext_b64, b);
	free(b);
	g_ciphertext_b64[out_len] = 0;

	return g_ciphertext_b64;
}

/** Returns zero if the session identifiers are equal. */
int compare_session_id(unsigned char* id1, unsigned char* id2)
{
	return memcmp(id1, id2, SESSION_ID_LEN);
}

/** Returns zero if the encryption keys are equal. */
int compare_encryption_key(unsigned char* key1, unsigned char* key2)
{
	return memcmp(key1, key2, ENCRYPTION_KEY_LEN);
}

void copy_session_id(unsigned char* src, unsigned char* dest)
{
	memcpy(dest, src, SESSION_ID_LEN);
}

void copy_encryption_key(unsigned char* src, unsigned char* dest)
{
	memcpy(dest, src, ENCRYPTION_KEY_LEN);
}

/* vim: set ts=4 sw=4 tw=0 noet :*/
